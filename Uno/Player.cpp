#ifndef PLAYER_CPP
#define PLAYER_CPP

#include "Cards.h"
#include "Uno.h"
#include "Player.h"
#include <ctime>
#include <vector>
#include <algorithm>

int Player::playerCount = 1;

Player::Player()
{
}

Player::Player(std::string _name, bool _isAI)
{
	name = _name;
	isAI = _isAI;
	playerNum = Player::playerCount++;
	std::vector<Card*> inHand;
	score = 0;
}

std::string Player::getName()
{
	return name;
}

int Player::getPlayerNum()
{
	return playerNum;
}

bool Player::getIsAI()
{
	return isAI;
}

void Player::addCard(Card *_card)
{
	inHand.push_back(_card);
}

void Player::setScore(int _score)
{
	score = _score;
}

int Player::getScore()
{
	return score;
}

#endif

