#ifndef REVCARD_C
#define REVCARD_C

#include "Cards.h"

//cards with a reverse face value
ReverseCard::ReverseCard(char _colour) : Card()
{
	points = 20;
	type = "reverse";
	colour = _colour;
	smallCardDrawing[4] = " | rev |";
	face = 'r';
	setConsoleColourNumber(colour);

}

std::string ReverseCard::drawCard()
{
	setCardColour(consoleColourNumber);

	std::ostringstream ss;
	std::string cardPic;

	ss << "    ____________ \n"
		<< "   |            |\n"
		<< "   |            |\n"
		<< "   |   _____    |\n"
		<< "   |  |  __ \\   |\n"
		<< "   |  | |__) |  |\n"
		<< "   |  |  _  /   |\n"
		<< "   |  | | \\ \\   |\n"
		<< "   |  |_|  \\_\\  |\n"
		<< "   |            |\n"
		<< "   |   REVERSE  |\n"
		<< "   |____________|\n\n";

	cardPic = ss.str();
	return cardPic;
}

ReverseCard::~ReverseCard()
{}

#endif