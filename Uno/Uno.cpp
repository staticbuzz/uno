//uno.cpp
#ifndef UNO_CPP
#define UNO_CPP

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <windows.h>
#include <stdlib.h>
#include "Cards.h"
#include "Player.h"
#include "Uno.h"
#include "Game.h"



using namespace std;

int main(){
	bool gameOn = true;
	int currentPlayer = 0;
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r); //stores the console's current dimensions
	MoveWindow(console, 0, 0, 800, 800, TRUE);

	showWelcome();
	
	while (gameOn = true)
	{
		char playAgain;
		Game game;
		system("cls");
		showWelcome();
		cout << "\n   Would you like to play again? (y/n) ";
		cin >> playAgain;
		if (playAgain == 'n')
			gameOn = false;
		else
			gameOn = true;
	}
	
	system("pause");

	return 0;
}

void showWelcome()
{
	ostringstream unoSS;
	Game::setTextColour(10);
	unoSS << "   ||     ||  ||\\\\    ||    ======    ||\n"
		<< "   ||     ||  || \\\\   ||   //    \\\\   ||\n"
		<< "   ||     ||  ||  \\\\  ||  ||      ||  ||\n"
		<< "    \\\\    //  ||   \\\\ ||   \\\\    //     \n"
		<< "     ======   ||    \\\\||    ======    ()";

	string uno = unoSS.str();
    unoSS.str("");
	unoSS << "                    //// \n"
		<< "                   (O O) \n"
		<< "     --------oOO----(_)---------------\n"
		<< "     I       by Chris Whitehead      I\n"
		<< "     I          Get Playing-         I\n"
		<< "     I              UNO!             I\n"
		<< "     ----------------------oOO--------\n"
		<< "                  I__I__I             \n"
		<< "                   II II              \n"
		<< "                  ooO Ooo             \n";

	
	string unoguy = unoSS.str();

	system("cls");
	cout << "\n\n" << uno << "\n\n";
	Game::setTextColour(9);
	cout << "\n\n" << unoguy << "\n\n";
	Game::setTextColour(7);
}






#endif








  
