#ifndef NUMCARD_C
#define NUMCARD_C

#include "Cards.h"
//Cards with numeric face value (ie 0-9)
NumberCard::NumberCard(char _face, char _colour) : Card()
{
	type = "normal";
	face = _face;
	colour = _colour;

	std::stringstream ss;
	ss << " |  " << face << "  |";
	smallCardDrawing[4] = ss.str();

	switch (face)
	{
	case '0' :
		points = 0;
		break;
	case '1' :
		points = 1;
		break;
	case '2' :
		points = 2;
		break;
	case '3' :
		points = 3;
		break;
	case '4' :
		points = 4;
		break;
	case '5' :
		points = 5;
		break;
	case '6':
		points = 6;
		break;
	case '7' :
		points = 7;
		break;
	case '8' :
		points = 8;
		break;
	case '9' :
		points = 9;
		break;
	}

	setConsoleColourNumber(colour);

}

std::string NumberCard::drawCard()
{
	setCardColour(consoleColourNumber);
	std::ostringstream ss;
	std::string cardPic;
	if (face == '0')
	{
		ss << "    ____________ \n"
			<< "   |            |\n"
			<< "   |            |\n"
			<< "   |    ___     |\n"
			<< "   |   / _ \\    |\n"
			<< "   |  | | | |   |\n"
			<< "   |  | | | |   |\n"
			<< "   |  | |_| |   |\n"
			<< "   |   \\___/    |\n"
			<< "   |            |\n"
			<< "   |    ZERO    |\n"
			<< "   |____________|\n\n";

	}
	else if (face == '1')
	{
		ss << "    ____________ \n"
			<< "   |            |\n"
			<< "   |            |\n"
			<< "   |     __     |\n"
			<< "   |    /  |    |\n"
			<< "   |   	 | |    |\n"
			<< "   |     | |    |\n"
			<< "   |     | |    |\n"
			<< "   |    |___|   |\n"
			<< "   |            |\n"
			<< "   |     ONE    |\n"
			<< "   |____________|\n\n";

	}
	else if (face == '2')
	{
		ss << "    ____________ \n"
			<< "   |            |\n"
			<< "   |            |\n"
			<< "   |    ___     |\n"
			<< "   |   |__ \\    |\n"
			<< "   |   	  ) |   |\n"
			<< "   |     / /    |\n"
			<< "   |    / /_    |\n"
			<< "   |   |____|   |\n"
			<< "   |            |\n"
			<< "   |    TWO     |\n"
			<< "   |____________|\n\n";

	}
	else if (face == '3')
	{
		ss << "    ____________ \n"
			<< "   |            |\n"
			<< "   |            |\n"
			<< "   |   _____    |\n"
			<< "   |  |___  \\   |\n"
			<< "   |   ___) |   |\n"
			<< "   |  |___ <    |\n"
			<< "   |   ___) |   |\n"
			<< "   |  |_____/   |\n"
			<< "   |            |\n"
			<< "   |   THREE    |\n"
			<< "   |____________|\n\n";

	}
	else if (face == '4')
	{
		ss << "    ____________ \n"
			<< "   |            |\n"
			<< "   |            |\n"
			<< "   |   _  _     |\n"
			<< "   |  | || |    |\n"
			<< "   |  | || |_   |\n"
			<< "   |  |_   __|  |\n"
			<< "   |    | |     |\n"
			<< "   |    |_|     |\n"
			<< "   |            |\n"
			<< "   |    FOUR    |\n"
			<< "   |____________|\n\n";

	}
	else if (face == '5')
	{
		ss << "    ____________ \n"
			<< "   |            |\n"
			<< "   |            |\n"
			<< "   |   _____    |\n"
			<< "   |  | ____|   |\n"
			<< "   |  | |__     |\n"
			<< "   |  |___ \\    |\n"
			<< "   |   ___) |   |\n"
			<< "   |  |____/    |\n"
			<< "   |            |\n"
			<< "   |   FIVE     |\n"
			<< "   |____________|\n\n";

	}
	else if (face == '6')
	{
		ss << "    ____________ \n"
			<< "   |            |\n"
			<< "   |            |\n"
			<< "   |      __    |\n"
			<< "   |     / /    |\n"
			<< "   |    / /_    |\n"
			<< "   |   | '_ \\   |\n"
			<< "   |   | (_) |  |\n"
			<< "   |    \\___/   |\n"
			<< "   |            |\n"
			<< "   |     SIX    |\n"
			<< "   |____________|\n\n";

	}
	else if (face == '7')
	{
		ss << "    ____________ \n"
			<< "   |            |\n"
			<< "   |            |\n"
			<< "   |   ______   |\n"
			<< "   |  |____  |  |\n"
			<< "   |      / /   |\n"
			<< "   |     / /    |\n"
			<< "   |    / /     |\n"
			<< "   |   /_/      |\n"
			<< "   |            |\n"
			<< "   |   SEVEN    |\n"
			<< "   |____________|\n\n";

	}
	else if (face == '8')
	{
		ss << "    ____________ \n"
			<< "   |            |\n"
			<< "   |            |\n"
			<< "   |    ___     |\n"
			<< "   |   / _ \\    |\n"
			<< "   |  | (_) |   |\n"
			<< "   |   > _ <    |\n"
			<< "   |  | (_) |   |\n"
			<< "   |   \\___/    |\n"
			<< "   |            |\n"
			<< "   |   EIGHT    |\n"
			<< "   |____________|\n\n";

	}
	else if (face == '9')
	{
		ss << "    ____________ \n"
			<< "   |            |\n"
			<< "   |            |\n"
			<< "   |    ___     |\n"
			<< "   |   / _ \\    |\n"
			<< "   |  | (_) |   |\n"
			<< "   |   \\__, |   |\n"
			<< "   |     / /    |\n"
			<< "   |	/_/     |\n"
			<< "   |            |\n"
			<< "   |    NINE    |\n"
			<< "   |____________|\n\n";

	}

	cardPic = ss.str();
	return cardPic;

}


NumberCard::~NumberCard()
{}

#endif