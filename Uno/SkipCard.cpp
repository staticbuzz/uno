#ifndef SKIPCARD_C
#define SKIPCARD_C

#include "Cards.h"

//cards with a skip turn face value
SkipCard::SkipCard(char _colour) : Card()
{
	points = 20;
	type = "skip";
	colour = _colour;
	smallCardDrawing[4] = " |skip |";
	face = 's';
	setConsoleColourNumber(colour);

}

std::string SkipCard::drawCard()
{
	setCardColour(consoleColourNumber);

	std::ostringstream ss;
	std::string cardPic;

	ss << "    ____________ \n"
		<< "   |            |\n"
		<< "   |            |\n"
		<< "   |    _____   |\n"
		<< "   |   / ____|  |\n"
		<< "   |  | (____   |\n"
		<< "   |   \\___  \\  |\n"
		<< "   |    ____) | |\n"
		<< "   |   |_____/  |\n"
		<< "   |            |\n"
		<< "   |    SKIP    |\n"
		<< "   |____________|\n\n";

	cardPic = ss.str();
	return cardPic;
}

SkipCard::~SkipCard()
{}

#endif