#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

#include "Cards.h"
#include "Uno.h"

class Player
{
public:
	Player();
	Player(std::string _name, bool _isAI);
	std::string getName();
	int getPlayerNum();
	bool getIsAI();
	void Player::addCard(Card *_card);
	//std::vector<Card> getHeldCards();
	std::vector<Card*> inHand;
	int getScore();
	void setScore(int _score);

private:
	static int playerCount;
	static std::vector<std::string> compNames;
	std::string name;
	bool isAI;
	int playerNum;
	int score;
	
};


#endif