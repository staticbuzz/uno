#ifndef GAME_H
#define GAME_H

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <windows.h>
#include <stdlib.h>
#include "Player.h"
#include "Cards.h"
#include "Uno.h"


class Game
{
public:
	Game();
	void populateDeck();
	void playTurn();
	void resetRound();
	void playRound();
	void setupPlayers();
	void dealInitialCards(); 
	void pickupCard();
	void specialPickup(unsigned _num);
	void playCard();
	void displayBoard();
	void selectCard(char _direction);
	void nextTurn();
	void displayLeaderboard();
	void calculateScores();
	void drawCardsInHand(unsigned _startPos);
	static void setTextColour(int txtColour);
    static int currentPlayer;
	int const MAX_AIS = 9;
	int const MAX_PLAYERS = 10;

private:
	char NumberToString(int number);
	int numPlayers, numAIs, numHumans;
	Card *activeCard;
	Card *selectedCard;
	Card *tempCard;
	std::vector<Card*> drawPile,playedPile;
	int selected, round;
	char choice;
	bool isRoundOver;
	bool isReversed;
	bool isDraw2;
	bool isDraw4;
	bool isSkip;
	bool isGameOver;
	bool playersOK;
	bool areCardsDealt;
	Player players[10];	
	HWND console;
	enum Colour { DBLUE = 1, GREEN, GREY, DRED, DPURP, BROWN, LGREY, DGREY, BLUE, LIMEG, TEAL, RED, PURPLE, YELLOW, WHITE };
};

#endif