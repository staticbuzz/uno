#ifndef D2CARD_C
#define D2CARD_C

#include "Cards.h"

//Cards with a draw 2 face value
Draw2Card::Draw2Card(char _colour) : Card()
{
	points = 20;
	type = "draw2";
	colour = _colour;
	smallCardDrawing[4] = " |draw2|";
	face = 'd';
	setConsoleColourNumber(colour);
}

std::string Draw2Card::drawCard()
{
	
	setCardColour(consoleColourNumber);
	
	std::ostringstream ss;
	std::string cardPic;

	ss << "    ____________ \n"
		<< "   |            |\n"
		<< "   |            |\n"
		<< "   |      ___   |\n"
		<< "   |    _|   |  |\n"
		<< "   |   | |   |  |\n"
		<< "   |   | |___|  |\n"
		<< "   |   |___|    |\n"
		<< "   |            |\n"
		<< "   |            |\n"
		<< "   |   DRAW 2   |\n"
		<< "   |____________|\n\n";

	cardPic = ss.str();
	return cardPic;
}

Draw2Card::~Draw2Card()
{}

#endif