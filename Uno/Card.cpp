#ifndef CARD_CPP
#define CARD_CPP
#include "Cards.h"
#include <iostream>
#include <string>
#include <sstream>
//This is an abstract class and cannot be instantiated. Only derived classes can be instantiated
Card::Card()
{


	isSelected = false;
	smallCardDrawing[1] = "  _____ ";
	smallCardDrawing[2] = " |     |";
	smallCardDrawing[3] = " |     |";
	smallCardDrawing[5] = " |     |";
	smallCardDrawing[6] = " |_____|";


	if (isSelected)
	{
		char dnarrow = 25;
		std::stringstream  ss, ss2;
		ss << "  " << dnarrow << dnarrow << dnarrow << dnarrow << dnarrow << " ";
		smallCardDrawing[0] = ss.str();
	}
	else
		smallCardDrawing[0] = "        ";

}


void Card::setSelected(bool _isSelected)
{
	isSelected = _isSelected;
	if (isSelected)
	{
		char dnarrow = 25;
		std::stringstream  ss, ss2;
		ss << "  " << dnarrow << dnarrow << dnarrow << dnarrow << dnarrow << " ";
		smallCardDrawing[0] = ss.str();
	}
	else
		smallCardDrawing[0] = "        ";
}


std::string Card::getSmallCardDrawing(int _line)
{
	return smallCardDrawing[_line];
}

char Card::getFace()
{
	return face;
}

char Card::getColour()
{
	return colour;
}

int Card::getConsoleColourNumber()
{
	return consoleColourNumber;
}

//sets consoleColourNumber, used by drawCard() to set the console text colour for drawing the card
void Card::setConsoleColourNumber(char _colour)
{
	switch (_colour)
	{
	case 'R':
		consoleColourNumber = 12;
		break;
	case 'B':
		consoleColourNumber = 9;
		break;
	case 'Y':
		consoleColourNumber = 14;
		break;
	case 'G':
		consoleColourNumber = 2;
		break;
	default:
		consoleColourNumber = 7;
		break;
	}
}


std::string Card::getCardType()
{
	return type;
}

int Card::getPoints()
{
	return points;
}

//change the console text colour
void Card::setCardColour(int txtColour)
{
	txtColour = (Colour)txtColour;
	HANDLE hcon = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hcon, txtColour);
}


Card::~Card()
{}

#endif