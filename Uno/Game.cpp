#ifndef GAME_CPP
#define GAME_CPP

#include "Cards.h"
#include "Uno.h"
#include "Player.h"
#include "Game.h"
#include <ctime>
#include <vector>
#include <algorithm>
#include <string>
#include <windows.h>
#include <stdlib.h>
#include <sstream>

int Game::currentPlayer = 0;

Game::Game()
{
	numPlayers = 0;
	selected = 0;
	isRoundOver = false;
	isReversed = false;
	isDraw2 = false;
	isDraw4 = false;
	areCardsDealt = false;
	console = GetConsoleWindow();
	srand((unsigned)time(NULL));
	setupPlayers();
	populateDeck();
	round = 0;
	resetRound();//ensure all variables reset
	while (!isGameOver)
	{
		playRound();
		resetRound(); // also deallocates all cards from heap and removes dangling pointers. 
	}
}

//draw the actual game elements to console (cards, player names, etc)
void Game::displayBoard()
{
	
	
	int setConsoleColourNumber = 7;
	int nameColour = currentPlayer + 10;
	//pick from only appropriate console text colours (if just from 1 - 10 we get a lot of bad colours). this way we get colours 9-15 and 2, 3 & 6.
	if (nameColour > 15)
		nameColour -= 15;
	if (nameColour == 1)
		nameColour = 6;
	system("cls");

	setTextColour(9);
	std::cout << "\n\n   * * * * * * * * * * * * * * * * * * *       "; 
	setTextColour(7);
	std::cout << "--ROUND " << round << "--\n";
	setTextColour(9);
	std::cout << "   *                                   *       ";
	setTextColour(nameColour);
	std::cout << "Good luck, " << players[currentPlayer].getName() << "\n";
	setTextColour(9);
	std::cout << "   *          ";
	setTextColour(nameColour);
	std::cout << "Player " << players[currentPlayer].getPlayerNum() << "'s turn";
	setTextColour(9);
	//draw player number frame correctly despite extra digit for Player 10.
	if (currentPlayer == 9)
		std::cout << "         *\n";
	else
		std::cout << "          *\n";
	std::cout << "   *                                   * \n";
	std::cout << "   * * * * * * * * * * * * * * * * * * *       ";
	setTextColour(7);
	std::cout << drawPile.size() << " cards left in deck\n   ";
	std::cout << "\n\n   Current game card:\n";
	std::cout << activeCard->drawCard();
	setTextColour(7);

	std::cout << "\n   Cards in your hand:\n\n";
	drawCardsInHand(0);
	setTextColour(7);

	selectedCard = players[currentPlayer].inHand.at(selected);
	std::cout << "   Selected card:\n";
	std::cout << selectedCard->drawCard();
	setTextColour(7);
}

//handles drawing small cards for each card in player's hand in rows of 9.
void Game::drawCardsInHand(unsigned _startPos)
{
	//no row of cards needed if we have already drawn all the cards in the player's hand.
	if (_startPos < players[currentPlayer].inHand.size())
	{
		unsigned j = 0;
		for (int i = 0; i < 8; i++)
		{
			for (j = 0 + _startPos; j < players[currentPlayer].inHand.size() && j < 9 + _startPos; j++)
			{
				//determine which card is currently selected and draw some arrows above the card, pointing down to it
				if (j == selected)
					players[currentPlayer].inHand.at(j)->setSelected(true);
				else
					players[currentPlayer].inHand.at(j)->setSelected(false);
				setTextColour(players[currentPlayer].inHand.at(j)->getConsoleColourNumber());

				std::cout << players[currentPlayer].inHand.at(j)->getSmallCardDrawing(i);
			}
			std::cout << "\n";
		}
		/*recusively calls itself with an offset start position (each row is offset by 9, needed to prevent the same cards 
		being drawn on each row) for each row that will need to be drawn to show all small cards of cards in player's hand.*/
		drawCardsInHand(_startPos + 9);
		std::cout << "\n";
	}
}

//place a card from player's card in hand onto the played pile and process any subsequent actions
void Game::playCard()
{
	//make sure the card can be played
	if (selectedCard->getColour() == activeCard->getColour()
		|| selectedCard->getFace() == activeCard->getFace()
		|| selectedCard->getCardType() == "wild2"
		|| selectedCard->getCardType() == "wild4")
	{
		activeCard = selectedCard;
		//if the card can be played, remove the card from the players hand and add to the played pile.
		players[currentPlayer].inHand.erase(players[currentPlayer].inHand.begin() + selected);
		playedPile.push_back(selectedCard);
		if (selectedCard->getCardType() == "reverse")
		{
			//the isReversed boolean will mean that currentPlayer is decremented on nextTurn() if true, or incremented if false.
			if (!isReversed)
				isReversed = true;
			else
				isReversed = false;
			if (players[currentPlayer].getIsAI() == true)
			{
				setTextColour(12);
				std::cout << "\n   " << players[currentPlayer].getName() << " is playing a reverse card! Keep an eye on that one...\n   ";
				setTextColour(7);
				system("pause");
			}
			nextTurn();
		}
		//this will set the isSkip boolean which, if true, means that on nextTurn() the player will receive a message saying their turn was skipped.
		else if (selectedCard->getCardType() == "skip")
		{
			isSkip = true;
			if (players[currentPlayer].getIsAI() == true)
			{
				setTextColour(12);
				std::cout << "\n   " << players[currentPlayer].getName() << " is playing a skip turn card! Is it a ploy?...\n   ";
				setTextColour(7);
				system("pause");
			}
			nextTurn();
		}
		//this will set the isDraw2 boolean which, if true, means that on nextTurn() the player will be forced to draw 2 cards.
		else if (selectedCard->getCardType() == "draw2")
		{
			isDraw2 = true;
			if (players[currentPlayer].getIsAI() == true)
			{
				setTextColour(12);
				std::cout << "\n   " << players[currentPlayer].getName() << " is playing a draw 2 card! Evil robot...\n   ";
				setTextColour(7);
				system("pause");
			}
			nextTurn();
		}
		//this will set the isDraw2 or isDraw4 boolean which, if true, means that on nextTurn() the player will be forced to draw 2 or 4 cards.
		else if (selectedCard->getCardType() == "wild4" || selectedCard->getCardType() == "wild2")
		{
			
			char colour;
			std::string colTxt;
			int randNum = rand() % 4;
			//if player is AI, randomly choose a colour for the wild card.
			if (players[currentPlayer].getIsAI() == true)
			{
				if (randNum == 0)
				{
					colour = 'R';
					colTxt = "red";
				}
				else if (randNum == 1)
				{
					colour = 'Y';
					colTxt = "yellow";
				}
				else if (randNum == 2)
				{
					colour = 'B';
					colTxt = "blue";
				}
				else
				{
					colour = 'G';
					colTxt = "green";
				}
			}
			else
			{
				//if human player, prompt to select colour for wild card
				std::cout << "\n   Select a colour: (r)ed, (g)reen, (b)lue, (y)ellow ";
				std::cin >> colour;
				if (colour == 'r')
					colour = 'R';
				else if (colour == 'y')
					colour = 'Y';
				else if (colour == 'b')
					colour = 'B';
				else if (colour == 'g')
					colour = 'G';
			}
			if (selectedCard->getCardType() == "wild4")
			{
				tempCard = new WildCard('!', colour);
				isDraw4 = true;
				if (players[currentPlayer].getIsAI() == true)
				{
					setTextColour(12);
					std::cout << "\n   " << players[currentPlayer].getName() << " is playing a wild draw 4 card... Run for the hills!\n";
					std::cout << "   'I like the colour " << colTxt << "', said " << players[currentPlayer].getName() << ", impishly...\n   ";
					setTextColour(7);
					system("pause");
				}
			}
			else if (selectedCard->getCardType() == "wild2")
			{
				tempCard = new WildCard('w', colour);
				isDraw2 = true;
				if (players[currentPlayer].getIsAI() == true)
				{
					setTextColour(12);
					std::cout << "\n   " << players[currentPlayer].getName() << " is playing a wild draw 2 card... Danger, danger!\n";
					std::cout << "   " << players[currentPlayer].getName() << " crunched some CPU cycles and settled on " << colTxt << "!\n   ";
					setTextColour(7);
					system("pause");
				}
			}
			
			activeCard = tempCard;
			nextTurn();
		}
		else
		{
			if (players[currentPlayer].getIsAI() == true)
			{
				setTextColour(12);
				std::cout << "\n   " << players[currentPlayer].getName() << " is playing a card. Clever robot...\n   ";
				setTextColour(7);
				system("pause");
			}
			nextTurn();
		}
	}
	else
	{
		//if the card cannot be played
		setTextColour(12);
		std::cout << "\n   Cannot play selected card at this point... Try another\n   ";
		setTextColour(7);
		system("pause");
	}

}

//player not playing a card, so take one from the draw pile to be added to player's cards in hand.
void Game::pickupCard()
{
	char dplay;
	if (drawPile.size() > 0)
	{
		/*if AI player and they have to draw a card, check if the drawn card can be played. If it can be, play it and do nextTurn(). 
		If not, add to cards in hand. If player is human and has to draw a card, check if playable and then prompt whether to play it or not*/
		if (players[currentPlayer].getIsAI() == true)
		{
			setTextColour(12);
			std::cout << "\n   " << players[currentPlayer].getName() << " has to draw a card. Boo hoo...\n   ";
			setTextColour(7);
			system("pause");
		}
		int randNum = rand() % drawPile.size();
		selectedCard = drawPile.at(randNum);
		players[currentPlayer].inHand.push_back(selectedCard);
		drawPile.erase(drawPile.begin() + randNum);
		selected = players[currentPlayer].inHand.size() - 1;
		displayBoard();

		if (selectedCard->getColour() == activeCard->getColour()
			|| selectedCard->getFace() == activeCard->getFace()
			|| selectedCard->getCardType() == "wild2"
			|| selectedCard->getCardType() == "wild4")
		{
			if (players[currentPlayer].getIsAI() == true)
			{
				playCard();
			}
			else
			{
				setTextColour(12);
				std::cout << "\n   Play draw card? y/n: ";
				setTextColour(7);
				std::cin >> dplay;
				if (dplay == 'y')
					playCard();
				else
				{
					nextTurn();
				}
			}
		}
		else
		{
			if (players[currentPlayer].getIsAI() == true)
			{
				setTextColour(12);
				std::cout << "\n   " << players[currentPlayer].getName() << " couldn't play the card he picked up. What a shame...\n   ";
				setTextColour(7);
				system("pause");
			}
			else
			{
				setTextColour(12);
				std::cout << "\n   Aw rats, your stuck with the card... \n   ";
				setTextColour(7);
				system("pause");
			}
			nextTurn(); 
		}
	}
	else
	{
		//if there are no more cards in the draw pile display a message and run nextTurn()
		setTextColour(12);
		std::cout << "\n   No more cards in deck! Next player's turn...\n   ";
		setTextColour(7);
		system("pause");
		nextTurn();
	}
}

//player cops draw 2 card (wild or regular) or draw 4 wild card. Pick up required number of cards and set selected card for each iteration as feedback to player.
void Game::specialPickup(unsigned _num)
{
	if (_num > drawPile.size())
	{
		_num = drawPile.size();
		if (_num == 0)
		{
			std::cout << "\n   No cards left to draw... How very fortunate for you!\n   ";
			system("pause");
		}

		else
		{
			std::cout << "\n   Can only draw " << _num << " card(s).. Lucky you!\n   ";
			system("pause");
		}
	}
	//for each number of cards passed in as a parameter, display a message and add a card from draw pile to player's cards in hand.
	for (unsigned i = 0; i < _num; i++)
	{
		int randNum;
		randNum = rand() % drawPile.size();
		selectedCard = drawPile.at(randNum);
		players[currentPlayer].inHand.push_back(selectedCard);
		drawPile.erase(drawPile.begin() + randNum);
		selected = players[currentPlayer].inHand.size() - 1;
		displayBoard();
		setTextColour(12);
		std::cout << "\n   Drawing card " << i + 1;
		setTextColour(7);
		std::cout << "\n   ";
		system("pause");
	}
}

//move selected card forward or backward one in the list of cards in player's hand
void Game::selectCard(char _direction)
{
	if (_direction == '+')
	{
		//explicitly cast 'selected' as unsigned as .size() is never negative number
		if ((unsigned)selected < players[currentPlayer].inHand.size() - 1)
			selected++;
		else
			selected = 0;
	}
	else if (_direction == '-')
	{
		if (selected > 0)
			selected--;
		else
			selected = players[currentPlayer].inHand.size() - 1;
	}
}

//create the players depending on number who are playing
void Game::setupPlayers()
{
	std::string compNames[] = { "Hoover", "Hal-9000", "Cortana", "Siri", "T-800", "Bender", "Fugitoid", "Data", "Megatron" };
	Game::setTextColour(7);
	playersOK = false;
	while (!playersOK)
	{
		std::cout << "   How many humans are playing? (1 - 10) ";
		std::cin >> numHumans;
		if (numHumans >= 1 && numHumans <= 10)
			playersOK = true;
		else
		{
			setTextColour(12);
			std::cout << "\n   Only 1 to 10 players allowed...\n   ";
			setTextColour(7);
			system("pause");
			system("cls");
			showWelcome();
		}
	}
	if (numHumans != 10)
	{
		
		if (numHumans > 1)
			std::cout << "   How many AIs do you want to play with you? (0 - " << MAX_PLAYERS - numHumans << ") ";
		else 
			std::cout << "   How many AIs do you want to play with you? (1 - " << MAX_PLAYERS - numHumans << ") ";
		std::cin >> numAIs;
		if (numAIs > MAX_PLAYERS - numHumans)
		{
			numAIs = MAX_PLAYERS - numHumans;
			setTextColour(12);
			std::cout << "\n   Max 10 players allowed. Only " << numAIs << " AIs will be joining the game.\n";
			setTextColour(7);
		}
		if (numHumans <= 1 && numAIs < 1)
		{
			setTextColour(12);
			std::cout << "\n   Min 2 players allowed. One AI will join in to make up the numbers.\n";
			setTextColour(7);
			numAIs = 1;
		}
		std::cout << std::endl << std::endl;
	}
	for (int i = 0; i < numHumans; i++)
	{
		std::string tempName;
		std::cout << "   Enter Player " << i + 1 << "'s name: ";
		std::cin >> tempName;
		players[i] = Player::Player(tempName, false);
	}
	std::cout << std::endl << std::endl;
	for (int i = 0; i < numHumans; i++)
	{
		setTextColour(10);
		std::cout << "   Ready " << players[i].getName() << "!" << std::endl;
	}
	std::cout << std::endl;
	for (int i = 0; i < numAIs; i++)
	{
		std::string tempName = compNames[i];
		players[numHumans + i] = Player::Player(tempName, true);
		setTextColour(3);
		std::cout << "   " << players[numHumans + i].getName() << " has joined the game.\n";
		setTextColour(7);
	}
	std::cout << "\n   ";
	numPlayers = numAIs + numHumans;
	system("pause");
}

//load pointers of cards into a polymorphic vector of pointers (drawPile).
void Game::populateDeck()
{
	char colourType[4] = { 'R', 'B', 'G', 'Y' };
	for (int i = 0; i < 4; i++)
	{
		char currentColour = colourType[i];
		//insert two cards each for face values 1 to 9 for specified colour
		for (int i = 0; i < 9; i++)
		{
			//convert the counter (which is an integer) plus one into a char for NumberCard constructor
			char facenum = Game::NumberToString(i + 1);
			drawPile.push_back(new NumberCard(facenum, currentColour));
			drawPile.push_back(new NumberCard(facenum, currentColour));
		}
		//insert the single non-numeric and zero cards into the deck for specified colour
		drawPile.push_back(new NumberCard('0', currentColour));

		//insert 2 of each non-wild, non-numeric card into the deck for specified colour
		for (int i = 0; i < 2; i++)
		{
			drawPile.push_back(new SkipCard(currentColour));  //skip turn card
			drawPile.push_back(new Draw2Card(currentColour));  //draw 2 card
			drawPile.push_back(new ReverseCard(currentColour));  //reverse card
		}
	}
	for (int i = 0; i < 4; i++)
	{
		//draw 4 wild cards face value is '!', standard wild cards face value is 'w'
		drawPile.push_back(new WildCard('!', '-'));
		drawPile.push_back(new WildCard('w', '-'));
	}
}

//deal the initial 7 cards to each player and flip one card from the draw pile to start the played pile.
void Game::dealInitialCards()
{
	int randNum;
	for (int i = 0; i < 7; i++)
	{
		for (int j = 0; j < numPlayers; j++)
		{
			randNum = rand() % drawPile.size();
			activeCard = drawPile.at(randNum);
			players[j].inHand.push_back(activeCard);
			drawPile.erase(drawPile.begin() + randNum);
		}
	}
	randNum = rand() % drawPile.size();
	activeCard = drawPile.at(randNum);
	while (activeCard->getCardType() != "normal")
	{
		randNum = rand() % drawPile.size();
		activeCard = drawPile.at(randNum);
	}
	drawPile.erase(drawPile.begin() + randNum);
	playedPile.push_back(activeCard);
}

//do the business end of the turn and call sub functions
void Game::playTurn()
{
	selected = 0;
	choice = ' ';
	
	while (!isRoundOver)
	{
		
		if (isSkip)
		{	
			displayBoard();
			setTextColour(12);
			std::cout << "\n   Poor " << players[currentPlayer].getName() << "! Your turn got skipped...\n   ";
			setTextColour(7);
			system("pause");
			isSkip = false;
			nextTurn();
		}
		else if (isDraw2)
		{
			displayBoard();
			setTextColour(12);
			std::cout << "\n   Bad luck! Draw two cards...\n   ";
			setTextColour(7);
			system("pause");
			specialPickup(2);
			isDraw2 = false;
			nextTurn();
		}
		else if (isDraw4)
		{
			displayBoard();
			setTextColour(12);
			std::cout << "\n   OUCH!! Pick 'em up - 1, 2, 3, 4 of them!\n   ";
			setTextColour(7);
			system("pause");
			specialPickup(4);
			isDraw4 = false;
			nextTurn();
		}
		else 
		{
			if (players[currentPlayer].getIsAI() == false)
			{
				displayBoard();
				std::cout << "\n   (";
				setTextColour(10);
				std::cout << "p";
				setTextColour(7);
				std::cout << ")lay, (";
				setTextColour(10);
				std::cout << "d";
				setTextColour(7);
				std::cout << ")raw, (";
				setTextColour(10);
				std::cout << "n";
				setTextColour(7);
				std::cout << ")ext, (";
				setTextColour(10);
				std::cout << "b";
				setTextColour(7);
				std::cout << ")ack: ";
				std::cin >> choice;
				if (choice == 'd')
				{
					pickupCard();
				}

				else if (choice == 'p')
				{
					//prevent human player from playing wild draw 4 card if other playable cards in hand
					bool isNonWildPlayable = false;
					if (selectedCard->getCardType() == "wild4")
					{
						for (unsigned i = 0; i < players[currentPlayer].inHand.size(); i++)
						{
							if (players[currentPlayer].inHand.at(i)->getCardType() == "wild2"
								|| players[currentPlayer].inHand.at(i)->getFace() == activeCard->getFace()
								|| players[currentPlayer].inHand.at(i)->getColour() == activeCard->getColour())
							{
								isNonWildPlayable = true;
							}
						}
					}

					if (!isNonWildPlayable)
						playCard();
					else
					{
						setTextColour(12);
						std::wcout << "\n   You can only play a wild draw 4 card if you can't play any other card...\n   ";
						setTextColour(7);
						system("pause");
					}
				}

				else if (choice == 'n')
				{
					selectCard('+');
				}

				else if (choice == 'b')
				{
					selectCard('-');
				}

			}
			else
			{
				bool hasAIPlayedOne = false;
				 //check each card in hand, in order, ror a playable card that is NOT wild 4.
				for (unsigned i = 0; i < players[currentPlayer].inHand.size(); i++)
				{
					if (players[currentPlayer].inHand.at(i)->getCardType() == "wild2"
						|| (players[currentPlayer].inHand.at(i)->getFace() == activeCard->getFace() )
						|| (players[currentPlayer].inHand.at(i)->getColour() == activeCard->getColour() && players[currentPlayer].inHand.at(i)->getCardType() != "wild4"))
					{
						hasAIPlayedOne = true;
						selected = i;
						selectedCard = players[currentPlayer].inHand.at(i);
						displayBoard();
						playCard();
						break;	
					}
				}
				//only if cards in hand have nothing else playable , then can AI play any wild 4 cards it might have in hand.
				if (hasAIPlayedOne == false)
				{
					for (unsigned i = 0; i < players[currentPlayer].inHand.size(); i++)
					{
						if (players[currentPlayer].inHand.at(i)->getCardType() == "wild4")
						{
							hasAIPlayedOne = true;
							selected = i;
							selectedCard = players[currentPlayer].inHand.at(i);
							displayBoard();
							playCard();
							break;
						}
					}
				}
				//if no cards to play, AI draws a card
				if (hasAIPlayedOne == false)
				{
					displayBoard();
					pickupCard();
				}
			}
		}
	}
}

//increment or decrement the current player
void Game::nextTurn()
{
	if (players[currentPlayer].inHand.size() == 0)
	{
		isRoundOver = true;
		displayLeaderboard();
	}
	else
	{
		selected = 0;
		if (!isReversed)
		{
			if (currentPlayer < numPlayers - 1)
				currentPlayer++;
			else
				currentPlayer = 0;
		}
		else
		{
			if (currentPlayer > 0)
				currentPlayer--;
			else
				currentPlayer = numPlayers - 1;
		}
	}
}

//change the console text colour
void Game::setTextColour(int txtColour)
{
	txtColour = (Colour)txtColour;
	HANDLE hcon = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hcon, txtColour);
}

//convert a number to its string representation
char Game::NumberToString(int number)
{
	std::ostringstream ss;
	ss << number;
	std::string numStr = ss.str();
	char numChar = numStr[0];
	return numChar;
}

//shows tally of scores at the end of each round and at the end of the game
void Game::displayLeaderboard()
{
	calculateScores();
	system("cls");
	setTextColour(10);
	std::cout << "\n\n";
	std::cout << "   * * * * * * * * * * * * * * * * * * * * * * * * *\n";
	std::cout << "   *                                               *\n";
	if (!isGameOver)
		std::cout << "   *                ROUND OVER  !!!!               *\n";
	else 
		std::cout << "   *               IT'S ALL OVER  !!!!             *\n";
	std::cout << "   *                                               *\n";
	std::cout << "   * * * * * * * * * * * * * * * * * * * * * * * * *\n";
	setTextColour(3);
	if (!isGameOver)
	{
		std::cout << "\n   The battle is won but the war rages on! This round goes to " << players[currentPlayer].getName();
		std::cout << "\n\n   The spoils of war so far (first to 500 wins): \n\n";
	}
	else
	{
		setTextColour(12);
		std::cout << "\n   Ultimately there can only be one victor... The winner is " << players[currentPlayer].getName() << "!";
		std::cout << "\n\n   The final scores are: \n\n";
	}
	setTextColour(7);
	for (int i = 0; i < numPlayers; i++)
	{
		if (players[i].inHand.size() == 0)
		{
			setTextColour(9);
		}
		std::cout << "   Player " << players[i].getPlayerNum() << " - " << players[i].getName() << ": " << players[i].getScore() << "\n";
		setTextColour(7);
	}
	std::cout << "\n   ";
	system("pause");
}

//calculate scores at the end of each round
void Game::calculateScores()
{
	int tally = 0;
	for (int i = 0; i < numPlayers; i++)
	{
		for (unsigned j = 0; j < players[i].inHand.size(); j++)
		{
			tally += players[i].inHand.at(j)->getPoints();
		}
	}
	for (int i = 0; i < numPlayers; i++)
	{
		if (players[i].inHand.size() == 0)
		{
			int total = players[i].getScore() + tally;
			players[i].setScore(players[i].getScore() + tally);
			if (total >= 500)
			{
				isGameOver = true;
			}
		}
	}
}

//clear and reset all appropriate variables and vectors for new round
void Game::resetRound()
{
	currentPlayer = 0;
	isRoundOver = false;
	isReversed = false;
	isSkip = false;
	isDraw2 = false;
	isDraw4 = false;
	areCardsDealt = false;
	selected = 0;	
	delete tempCard;
	selectedCard = nullptr;
	activeCard = nullptr;
	tempCard = nullptr;

	for (int i = 0; i < numPlayers; i++)
	{
		for (unsigned j = 0; j < players[i].inHand.size(); ++j) 
		{
			delete players[i].inHand[j]; // Calls destructor for each card and deallocates the memory from heap
		}
		players[i].inHand.clear(); // removes the now-dangling pointers to cards that have been deallocated
	}
	for (unsigned i = 0; i < playedPile.size(); ++i)
	{
		delete playedPile[i]; // Calls destructor for each card and deallocates the memory from heap
	}
	playedPile.clear();  // removes the now-dangling pointers to cards that have been deallocated
	for (unsigned i = 0; i < drawPile.size(); ++i) 
	{
		delete drawPile[i]; // Calls destructor for each card and deallocates the memory from heap
	}
	drawPile.clear(); // removes the now-dangling pointers to cards that have been deallocated

}

//call all subprocesses for each round
void Game::playRound()
{
	round++;
	populateDeck();
	dealInitialCards();
	playTurn();
}	

#endif

