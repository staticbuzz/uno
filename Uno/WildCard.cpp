#ifndef WILDCARD_C
#define WILDCARD_C

#include "Cards.h"

//Cards with a wild face value (ie draw 2 or draw 4)
WildCard::WildCard(char _face, char _colour) : Card()
{
	points = 50;
	face = _face;
	colour = _colour;
	if (face == '!')
	{
		type = "wild4";
		smallCardDrawing[4] = " |wild4|";
	}
	else
	{
		type = "wild2";
		smallCardDrawing[4] = " |wild2|";
	}

	setConsoleColourNumber(colour);

}

std::string WildCard::drawCard()
{
	setCardColour(consoleColourNumber);

	std::ostringstream ss;
	std::string cardPic;
	if (face == '!')
	{
		ss << "    ____________ \n"
			<< "   |            |\n"
			<< "   | WILD   ___ |\n"
			<< "   |      _|   ||\n"
			<< "   |    _| |   ||\n"
			<< "   |  _| | |___||\n"
			<< "   | | | |___|  |\n"
			<< "   | | |___|    |\n"
			<< "   | |___|      |\n"
			<< "   |       WILD |\n"
			<< "   |   DRAW 4   |\n"
			<< "   |____________|\n\n";
	}
	else
	{
		ss << "    ____________ \n"
			<< "   |            |\n"
			<< "   | WILD       |\n"
			<< "   |      ___   |\n"
			<< "   |    _|   |  |\n"
			<< "   |   | |   |  |\n"
			<< "   |   | |___|  |\n"
			<< "   |   |___|    |\n"
			<< "   |            |\n"
			<< "   |       WILD |\n"
			<< "   |   DRAW 2   |\n"
			<< "   |____________|\n\n";
	}
	cardPic = ss.str();
	return cardPic;
}

WildCard::~WildCard()
{}

#endif